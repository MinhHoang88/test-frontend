import React from "react";

function UserList({ users }) {
  return (
    <div className="user-list mt-5">
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-4">
        {users.map((user) => (
          <div key={user.id} className="">
            <div>Name: {user.name}</div>
            <div>Email: {user.email}</div>
            <div>Phone: {user.phone}</div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default UserList;
