// App.js
import React, { useState, useEffect } from "react";
import "./App.css";
import UserList from "./components/UserList";

function App() {
  const [users, setUsers] = useState([]);
  const [filterUsers, setFilterUsers] = useState([]);
  const [searchUser, setSearchUser] = useState("");

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((response) => response.json())
      .then((data) => {
        setUsers(data);
        setFilterUsers(data);
      })
      .catch((error) => console.log(error));
  }, []);

  const handleSearch = (event) => {
    setSearchUser(event.target.value);
    const filteredResults = users.filter((user) =>
      user.name.toLowerCase().includes(event.target.value.toLowerCase())
    );
    setFilterUsers(filteredResults);
  };

  return (
    <div className="App px-5 md:px-12 py-20">
      <h1 className="my-4 font-bold text-lg">User List</h1>
      <input
        type="text"
        placeholder="Search by name..."
        value={searchUser}
        onChange={handleSearch}
        className="border border-gray-300 rounded px-4 py-2 mb-4 focus:outline-none focus:border-blue-500 p-2 mb-2"
      />
      <UserList users={filterUsers} />
    </div>
  );
}

export default App;
